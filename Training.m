function [model, meanX, eigenVectors] = Training(trainingImages, trainingLabels,PREPROCESSING_METHOD,PREPROCESSING_VALUE, FEATURE_EXTRACTION_METHOD, FEATURE_EXTRACTION_VALUE, CLASSIFICATION_METHOD)
%% Step 1 : Training
%%%%%%%%%%%%%%%%%%%%
fprintf('|-------------------|\n');
fprintf('| Step 1 : Training |\n');
fprintf('|-------------------|\n\n');

[numberOfTrainingImages, sizeOfOneTrainingImage] = size(trainingImages);
numberOfFaces = 0;
numberOfNon = 0;

% Calculate what proportion of our training set are faces and non-faces
for i = 1 : numberOfTrainingImages
    if trainingLabels(i) == 1
        numberOfFaces = numberOfFaces + 1;
    else
        numberOfNon = numberOfNon + 1;
    end
end

disp(['Number of Training Images: ',num2str(numberOfTrainingImages)]);
disp(['Number of Training Faces: ',num2str(numberOfFaces)]);
disp(['Number of Training Non-faces: ',num2str(numberOfNon)]);

% Visualise some images to ensure they're loaded correctly
DisplayImages(trainingImages, 100, 10, 10, 'Training Images');

% Apply Preprocessing techniques to each image
preprocessedTrainingImages = zeros(numberOfTrainingImages, sizeOfOneTrainingImage);
for index = 1:numberOfTrainingImages
     preprocessedTrainingImages(index,:) = PreprocessImage(trainingImages(index,:), PREPROCESSING_METHOD, PREPROCESSING_VALUE, true);
end

% Visualise some images to ensure they were preprocessed correctly
DisplayImages(preprocessedTrainingImages, 40, 4, 10, 'Preprocessed Training Images');

% Get featureExtractedTrainingImages to be used to train the model
for index = 1:numberOfTrainingImages
     featureExtractedTrainingImages(index,:) = FeatureExtraction(preprocessedTrainingImages(index,:), FEATURE_EXTRACTION_METHOD, FEATURE_EXTRACTION_VALUE);
end

if ~strcmp(FEATURE_EXTRACTION_METHOD, 'Gabor')
    % Only do dimensionality reduction if it's not Gabor - Takes too long
    [eigenVectors, eigenvalues, meanX, Xpca] = PrincipalComponentAnalysis (featureExtractedTrainingImages, 50);
    modelSVM = SVMtraining(Xpca, trainingLabels);
    PlotSVM(trainingLabels, Xpca, modelSVM, eigenVectors);
    model = GetModel(Xpca, trainingLabels, CLASSIFICATION_METHOD);
else
    model = GetModel(featureExtractedTrainingImages, trainingLabels, CLASSIFICATION_METHOD);
    meanX = 0;
    eigenVectors = 0;
end

%DisplayImages(featureExtractedTrainingImages, 40, 4, 10, 'Training Images');

% Get the model

end 