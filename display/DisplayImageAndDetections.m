function DisplayImageAndDetections(Picture,Objects)
%DisplayImageAndDetections(Picture,Objects)
%   Draws the input picture and all (Bounding Box) Objects on top of it

% Show the picture
figure, imagesc(Picture), colormap(gray), hold on;

% Show the detected objects
% If there are objects
if(~isempty(Objects))
    % For each of the objects
    for n=1:size(Objects,1)
        width = Objects(n,3); 
        height = Objects(n,4);
        weight = Objects(n,5);
        
        minX = Objects(n,1);
        minY = Objects(n,2);
        
        %Colour codes the bounding box according to confidence weighting
        if (weight < 2 )
            rectangle('Position', [minX,minY,width,height],'EdgeColor','b','LineWidth',2 );
        elseif (weight < 5)
            rectangle('Position', [minX,minY,width,height],'EdgeColor','y','LineWidth',2 );
        else
            rectangle('Position', [minX,minY,width,height],'EdgeColor','r','LineWidth',2 );
        end
    end
end
