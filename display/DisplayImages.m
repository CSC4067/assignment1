function DisplayImages( images, amount, rows, columns, figureName )
%DisplayImages( images, amount, rows, columns, figureName )

figure('name', figureName)
for index = 1:amount
    % reshape matrix for display
    vector = reshape(images(index,:),[27 18]);
    Im = mat2gray(vector);
    subplot(rows,columns,index), imshow(Im), colormap(gray), title(['image ', num2str(index)]);
end
end

