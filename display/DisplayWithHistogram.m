function [] = DisplayWithHistogram( image )

figure
subplot(1,2,1), imshow(image), title('image');
subplot(1,2,2), imhist(image), title('histogram');

end

