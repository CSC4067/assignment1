function PlotSVM( labels, reducedSpace, modelSVM, U )

figure, hold on
colours= ['r.'; 'g.'; 'b.'; 'k.'; 'y.'; 'c.'; 'm.'; 'r+'; 'g+'; 'b+'; 'k+'; 'y+'; 'c+'; 'm+'];
count=0;

for i=min(labels):max(labels)
    count = count+1;
    indexes = find (labels == i);
    plot3(reducedSpace(indexes,1),reducedSpace(indexes,2),reducedSpace(indexes,3),colours(count,:))
end

hold on

%transformation to the full image to the best 3 dimensions 
imean=mean(reducedSpace,1);
xsup_pca=(modelSVM.xsup-ones(size(modelSVM.xsup,1),1)*imean)*U(:,1:50)';

% plot support vectors
h=plot3(xsup_pca(:,1),xsup_pca(:,2),xsup_pca(:,3),'go');
set(h,'lineWidth',5)

end