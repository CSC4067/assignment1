clear all
close all

image = imread('resources/im1.jpg');

imagesc(image);
colormap(gray);

image = PixelBasedThresholding(image, 150);
figure
imagesc(image);
colormap(gray);

[trainingImages, trainingLabels] = loadFaceImages('face_train.cdataset');

image = trainingImages(1,:);
image = reshape(image,[27 18]);
figure;
imagesc(image);
colormap(gray);

image = PixelBasedThresholding(image, 150);
figure;
imagesc(image);
colormap(gray);
