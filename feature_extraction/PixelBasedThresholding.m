function [ after ] = PixelBasedThresholding( before, threshold )
% PixelBasedThresholding
% Takes in a matrix of an image and returns a black/white version of it
% based off of the threshold.
% Pixels with values higher than or equal to the threshold are set to 255,
% whereas values lower are set to 0
h = size(before,1);
w = size(before,2);
after = zeros(h, w);

for i = 1 : h
    for j = 1 : w
        if before(i,j) >= threshold
            after(i,j) = 255;
        else
            after(i,j) = 0;
        end
    end
end

