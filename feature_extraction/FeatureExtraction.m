function [ extractedFeatures ] = FeatureExtraction( image, method, value )
%FeatureExtraction Applys a feature extraction method to an image and
%returns the processed image
%   function [ featuresExtracted ] = FeatureExtraction( image, method, value )
%
%   image:  the image to have feature extraction applied to
%   method: the method of feature extraction to be used ['Gabor', 'Pixel',
%           'Dim', 'Raw']
%   value:  the value to be used alongside the chosen method - dimensions
%           to reduce for Dim, threshold value for Pixel
%
%   extractedFeatures:  The image after it has been processed with the
%                       chosen method

extractedFeatures = image;

if ~exist('value', 'var')
    % Ensure value is set before it's used.
    value = 0;
end

image = reshape(image,[27 18]);
% image = image .* 255;
if strcmp(method,'Gabor')
    extractedFeatures = gabor_feature_vector(image);
elseif strcmp(method,'Pixel')
    extractedFeatures = PixelBasedThresholding(image,value);
    extractedFeatures = GreyscaleImageToVector(extractedFeatures);
elseif strcmp(method,'HoG')
   [extractedFeatures,histogramVisualisation] = extractHOGFeatures(image, 'CellSize', [9,6]);
   % code for plotting HoG over image for testing purposes
%    figure;
%    imagesc(image); colormap(gray);
%    hold on;
%    plot(histogramVisualisation);
end    

end

