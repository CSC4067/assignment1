function [ vectors ] = GaborVectors( images )
%GABOR Processes each of the images and returns their vectors
%   Takes in an array of images then produces the vector for each of the
%   images and adds it to a matrix which is returned.
numImages = size(images,1);
vectors = zeros(numImages, 19440);

for i = 1 : numImages
    image = reshape(images(i,:),[27 18]);
    vector = gabor_feature_vector(image);
    vectors(i,:) = vector;
end