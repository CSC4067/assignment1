function [ duplicate ] = Duplicate (boundingBox1, boundingBox2)
    %% a bounding box is a duplicate if it overlaps by at least 50%
    
%     % bounding box 1 dimensions
%     BB1xmin = boundingBox1(:,:,1);
%     BB1ymin = boundingBox1(:,:,2);
%     BB1xmax = boundingBox1(:,:,3);
%     BB1ymax = boundingBox1(:,:,4);
%     
%     BB1width = abs(BB1xmax - BB1xmin);
%     BB1height = abs(BB1ymax - BB1ymin);
%     BB1midpointX = abs(BB1xmax - BB1width/2);
%     BB1midpointY = abs(BB1ymax - BB1height/2);
%      
%     % bounding box 2 dimensions
%     BB2xmin = boundingBox2(:,:,1);
%     BB2ymin = boundingBox2(:,:,2);
%     BB2xmax = boundingBox2(:,:,3);
%     BB2ymax = boundingBox2(:,:,4);
%     
%     BB2width = abs(BB2xmax - BB2xmin);
%     BB2height = abs(BB2ymax - BB2ymin);
%     BB2midpointX = abs(BB2xmax - BB2width/2);
%     BB2midpointY = abs(BB2ymax - BB2height/2);
    
% need to ensure width and height of bounding boxes are 'positive'
    
duplicate = 0;
overlapRatio = rectint(boundingBox1(:).', boundingBox2(:).');
%overlapRatio = bboxOverlapRatio(boundingBox1(:).', boundingBox2(:).');

if( overlapRatio >= 50 )
    duplicate = 1;
end

end