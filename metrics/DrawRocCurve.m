function [AUC] = DrawRocCurve( predictions, labels )
%DRAWROCCURVE 
figure('name', 'ROC Curve')

% From example on matlab
[X,Y,T,AUC] = perfcurve(labels, predictions, 1);


plot(X,Y)
xlabel('False positive rate')
ylabel('True positive rate')
title('ROC for Classification by Logistic Regression')

end

