function [ Recall, Precision, Specificity, FMeasure, FalseAlarm, BasicAccuracy ] ...
    = ErrorMeasure( predictions, labels )
%ERRORMEASURE Calculates the Recall, Precision, Specificity, FMeasure and
%False Alarm of the predictions
%   Iterates through all of the predictions and compares the predicted
%   value with the actual label.
%   If the prediction was correct, depending on what was predicted either
%   tp (True Positive) or tn (True Negative) will be incremented.
%   If the prediction was incorrect, depending on what was predicted either
%   fp (False Positive) or fn (False Negative) will be incremented.
%   These values are then used to calculate the values for Recall,
%   Precision, Specificity, FMeasure, and FalseAlarm.
%
%   Recall = # of found positives / # of positives
%   Precision = # of found positives / # of found
%   Specificity = tn/(tn+fp)
%   F-measure(F1) = 2x(precision x recall)/(precision + recall)
%   False alarm rate = 1 - Specificity 

numPredictions = size(predictions,1);
numLabels = size(labels,1);

tp=0;
tn=0;
fp=0;
fn=0;

if numPredictions ~= numLabels
    % We don't have the same number of predictions as labels, so exiting 
    % the method early before an out of bounds error occurs
    return
end

for i = 1 : numPredictions
    prediction = predictions(i);
    label = labels(i);
    
    if prediction == label
        if prediction == 1
            % Correct Prediction - It was a face
            tp = tp + 1;
        else
            % Correct Prediction - It wasn't a face
            tn = tn + 1;
        end
    else
        if prediction == 1
            % Incorrect Prediction - It wasn't a face
            fp = fp + 1;
        else
            % Incorrect Prediction - It was a face
            fn = fn + 1;
        end
    end
end

% #true negative + #true positive / #total 
BasicAccuracy = (tn + tp)/(tp + tn + fp + fn);
%  Recall = # of found positives / # of positives
Recall = tp/(tp+fn);
% Precision = # of found positives / # of found
Precision = tp/(tp+fp);
% Specificity = tn/(tn+fp)
Specificity = tn/(tn+fp);
% F-measure(F1) = 2x(precision x recall)/(precision + recall)
FMeasure = (2 * tp) / ((2 * tp) + fn + fp);
% False alarm rate = 1 - Specificity 
FalseAlarm = 1 - Specificity;

end

