function [Recall, Precision, Specificity, FMeasure, FalseAlarm, BasicAccuracy] = Testing(testingImages, testingLabels, model, K, PREPROCESSING_METHOD,PREPROCESSING_VALUE, FEATURE_EXTRACTION_METHOD, FEATURE_EXTRACTION_VALUE, CLASSIFICATION_METHOD, meanX, eigenVectors)
%% Step 2 : Testing
%%%%%%%%%%%%%%%%%%%
fprintf('\n|------------------|\n');
fprintf('| Step 2 : Testing |\n');
fprintf('|------------------|\n\n');

[numberOfTestingImages, sizeOfOneTestingImage] = size(testingImages);

numberOfFaces = 0;
numberOfNon = 0;

% Calculate what proportion of our testing set are faces and non-faces
for i = 1 : numberOfTestingImages
    if testingLabels(i) == 1
        numberOfFaces = numberOfFaces + 1;
    else
        numberOfNon = numberOfNon + 1;
    end
end

disp(['Number of Testing Images: ',num2str(numberOfTestingImages)]);
disp(['Number of Testing Faces: ',num2str(numberOfFaces)]);
disp(['Number of Testing Non-faces: ',num2str(numberOfNon)]);

% Preprocess each of our Testing Images
preprocessedTestingImages = zeros(numberOfTestingImages, sizeOfOneTestingImage);
for index = 1 : numberOfTestingImages
    preprocessedTestingImages(index,:) = PreprocessImage(testingImages(index,:), PREPROCESSING_METHOD, PREPROCESSING_VALUE, true);
end
% Visualise some images to ensure they were preprocessed correctly
DisplayImages(preprocessedTestingImages, 40, 4, 10, 'Preprocessed Testing Images');

% Get featureExtractedTestingImages to be used to train the model
for index = 1:numberOfTestingImages
     thisImage = FeatureExtraction(preprocessedTestingImages(index,:), FEATURE_EXTRACTION_METHOD, FEATURE_EXTRACTION_VALUE);
     if ~strcmp(FEATURE_EXTRACTION_METHOD, 'Gabor')
         featureExtractedTestingImages(index,:) = (thisImage - meanX) * eigenVectors;
     else
         featureExtractedTestingImages(index,:)  = thisImage;
     end
end

% Get our predictions
[ predictions ] = GetPredictions( featureExtractedTestingImages, model, CLASSIFICATION_METHOD, K );

% Calculate how good our classifier was
[ Recall, Precision, Specificity, FMeasure, FalseAlarm, BasicAccuracy ] = ErrorMeasure( predictions(:,1), testingLabels );

% Display some of the incorrectly classified images
wrongIndex = zeros(40,1);
total = 1;
for i = 1 : size(predictions(:,1))
    if (predictions(i,1) ~= testingLabels(i))
        if total <= 40
            wrongIndex(total) = i;
            total = total + 1;
        end
    end
end

wrongIndex( ~any(wrongIndex,2), : ) = [];
wrongImages = zeros(size(wrongIndex,1), sizeOfOneTestingImage);

for i = 1 : size(wrongIndex,1)
    j = wrongIndex(i);
    wrongImages(i,:) = testingImages(j,:);
end

DisplayImages(wrongImages,size(wrongImages,1), 4, 10, 'Wrongly Classified');
end 