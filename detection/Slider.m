function [ boundingBoxes ] = Slider( image, model, classificationMethod, K, featureExtractionMethod, featureExtractionValue, preprocessingMethod, preprocessingValues, meanX, eigenVectors)
%SCALINGSLIDER takes in an image and a model and classification method
% then scans across it in order to detect any matches to the model within 
% the image.
%
%  function [ boundingBoxes ] = ScalingSlider( image, model, classificationMethod, K)
%
%  Takes in an image, model, classificationMethod, and K value
%  Then scans across it using a sliding window which scales to different 
%  sizes in order to detect if there are any objects that match the model 
%  within the image. 
% 
%  Returns objects that were found.

% get image height and width, from this we will choose our sizes of
% sampling window
%[imageHeight, imageWidth] = size(image);

% training images were 27 x 18, therefore try and keep sample sizes
% relative (and scalable) to these dimensions. This will stop samples being
% distorted when rescaling them to get a prediction.
samplingX = 12;
samplingY = 18;

% how fast we grow the sample size
changeInSampleSizeX = 2;
changeInSampleSizeY = 3;

% sampling sizes will max out at maxSamplingX x maxSamplingY.
maxSamplingX = samplingX * 5/3;
maxSamplingY = samplingY * 5/3;

boundingBoxes = [];

index = 1;
while( samplingX <= maxSamplingX && samplingY <= maxSamplingY )
    for r = 1:round(samplingX/6): size(image,2)
       for c = 1:round(samplingY/6): size(image,1)
           if(c+samplingY-1 <= size(image,1) && r+samplingX-1 <= size(image,2))
                % crop the sample
                sampleIm = image(c:c+samplingY-1,r:r+samplingX-1);
                %imagesc(sampleIm, [0,255]); colormap(gray);

                % all the training images were 27x18 matrix so we need
                % to reshape
                sampleIm = imresize(sampleIm, [27 18]);
                % sampleIm = im2double(sampleIm);
                
                % apply preprocessing to the sample
                preprocessedImage = PreprocessImage(sampleIm, preprocessingMethod, preprocessingValues);

                %Get the GaborVector for this sample to be used to classify
                %it
                featureExtractedImage = FeatureExtraction(preprocessedImage,featureExtractionMethod,featureExtractionValue);
                featureExtractedImage = featureExtractedImage(:)';

                % apply dimensionality reduction
                Xpca = (featureExtractedImage - meanX) * eigenVectors;

                if size(featureExtractedImage,1) > 1
                    %we reshape the digit into a vector for preprocessing
                    %etc.
                    featureExtractedImage = featureExtractedImage(:)';
                end
                % get a prediction, i.e. is this a face or not
                if strcmp(classificationMethod, 'SVM')
                    [prediction, weight] = SVMTesting(Xpca,model);
                elseif strcmp(classificationMethod, 'KNN')
                    % Ensure K exists before we attempt to use it
                    if ~exist('K', 'var')
                        % If it didn't exist we will set it to 1 and predict using NN
                        K = 1;
                    end
                    [prediction, weight]= KNNTesting(Xpca,model,K);
                elseif  strcmp(classificationMethod, 'NN')
                    K = 1;
                    [prediction, weight] = KNNTesting(featureExtractedImage,model,K);
                end
                % store the bounding box of this sample if it is
                % predicted to be a face
                 if( prediction == 1)
                    xmin = r;
                    ymin = c;
                    xmax = r + samplingX;
                    ymax = c + samplingY;

                    width = xmax - xmin;
                    height = ymax - ymin;
%                     midpointX = xmax - width/2;
%                     midpointY = ymax - height/2;
                   
                    boundingBoxes(index,:) = [xmin ymin width height weight];
                    index = index + 1;
                                       
%                     boundingBox(1,:) = [xmin ymin width height weight];
%                     DisplayImageAndDetections(image, boundingBox);
%                     pause;
                 end
           end
       end
    end
     samplingX = samplingX + changeInSampleSizeX;
     samplingY = samplingY + changeInSampleSizeY;
end

end

