function [ merged ] = NMS( boxes )
%NMA Summary of this function goes here
%   Detailed explanation goes here

numberOfBoxes = size(boxes);
merged = boxes;
  
for i = 1:numberOfBoxes
   if isequal(boxes(i,1:4),[0,0,0,0])
       % This row has been removed so comparisons are not necessary
       continue
   end
   % Start at i+1 to prevent unnecessary comparisons
    for j = i+1 : numberOfBoxes
        if isequal(boxes(j,1:4),[0,0,0,0])
            % This row has been removed so comparisons are not necessary
            continue
        end
       % If this is a duplicate then remove
       if( Duplicate(boxes(i,1:4), boxes(j,1:4)) )
           % Compare their weights and remove the less confident box
           if boxes(i,5) > boxes(j,5)
                merged(j,:) = [0,0,0,0,0];
           else
                merged(i,:) = [0,0,0,0,0];
                % Stop making comparisons using i as it's been removed
                break
           end
           
       end
    end
end

% Remove all empty rows
merged( ~any(merged,2), : ) = [];

end

