function [ predictions ] = GetPredictions( images, model, method, K )
%[ predictions ] = GetPredictions( images, model, method )
%   
%   images:     The images to be tested,
%   model:      The model to test the images against,
%   method:     The method of testing [SVM, NN, KNN]
%   K:          The value of K if the method is KNN
%  
%   Tests each of the passed in images and returns the predictions for each
%   image.

numberOfImages = size(images, 1);
predictions = zeros(numberOfImages,2);

for index = 1:numberOfImages
    % Use our training model to predict what the image is and record the
    % prediction
    if strcmp(method,'SVM')
        [prediction, weight]  = SVMTesting(images(index,:), model);
    elseif strcmp(method,'NN')
        K = 1;
        [prediction, weight]  = KNNTesting(images(index,:), model,K);
    elseif strcmp(method,'KNN')
                % Ensure K exists before we attempt to use it
        if ~exist('K', 'var')
            % If it didn't exist we will set it to 1 and predict using NN
            K = 1;
        end
        [prediction, weight] = KNNTesting(images(index,:), model,K);
    end
        
    predictions(index,:) = [prediction, weight];
end


end

