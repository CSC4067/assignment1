function [prediction, weight] = KNNTesting(testImage, modelNN, K)

modelImages = size(modelNN.neighbours,1);
results = zeros(modelImages,2);

for i=1:modelImages
    ed = EuclideanDistance(testImage, modelNN.neighbours(i,:));
    results(i,1) = ed;
    results(i,2) = modelNN.labels(i);
end

% Sort the rows, then select a subsection of them. 
sorted = sortrows(results,1);
closest = sorted(1:K,2);
% Mode returns the item which occurs most frequently in the vector
prediction = mode(closest);
if( prediction == -1)
    weight = 0;
else
   weight = 1/mean(sorted(1:K,1));
end