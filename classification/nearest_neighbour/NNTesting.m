function prediction = NNTesting(testImage,modelNN)
minDistance = EuclideanDistance(testImage,modelNN.neighbours(1,:));
NNindex = 1;
for index = 2:size(modelNN)
     distance = EuclideanDistance(testImage,modelNN.neighbours(index,:));
     if (distance<minDistance)
         minDistance = distance;
         NNindex = index;
     end
end
prediction = modelNN.labels(NNindex);
end

