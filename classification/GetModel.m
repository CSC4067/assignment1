function model = GetModel( images, labels, method )
% model = GetModel( images, model, method )
%   
%   images:     The images to be trained with,
%   labels:     The labels for the images
%   method:     The method of testing [SVM, NN, KNN]
%  
%   Produces a model using the testing images and labels for the given
%   method

if strcmp(method,'SVM')
    model = SVMtraining(images, labels);
elseif strcmp(method,'KNN')
    model = NNtraining(images, labels);
elseif strcmp(method,'NN')
    model = NNtraining(images, labels);
end

