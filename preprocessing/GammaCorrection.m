function [ image ] = GammaCorrection (image,gamma)
    
    image = mat2gray(image);
    image = imadjust(image,[],[],gamma);
    image = image .* 255;

end

