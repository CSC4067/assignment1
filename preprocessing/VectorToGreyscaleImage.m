function [ image ] = VectorToGreyscaleImage (image)

vector =  reshape(image,[27 18]);
image = mat2gray(vector);
image = image .* 255;

end
