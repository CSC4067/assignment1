function [ image ] = GammaCorrectionSD (image,gamma)
    
    image = mat2gray(image);
    image = imadjust(image,gamma,[]);
    image = image .* 255;

end