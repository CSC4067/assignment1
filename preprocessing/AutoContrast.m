function [ image ] = AutoContrast(image)
    image = mat2gray(image);
    image = imadjust(image);
    image = image .* 255;
end

