function [ image ] = HistogramEqualisation( image )

image = mat2gray(image);
image = histeq(image);
image = image .* 255;

end

