function [ image ] = Brightening( image, shift )
%BRIGHTENING Used to change the brightness of the image
%   Modifies and image by applying the 'shift' to each of the pixel values
%   Use a negative shift value to make the image darker.

image = image + shift;

for i = 1 : size(image,1)
    for j = 1 : size(image,2)
        if image(i,j) < 0
            image(i,j) = 0;
        elseif image(i,j) > 255
            image(i,j) = 255;
        end
    end
end

