function [ image ] = PreprocessImage( image, methods, values, vector )
%[ predictions ] = PreprocessImage( image, method, value )
%   
%   image:      The image to be preprocessed,
%   method:     The method of testing [Gamma, Hist, Bright]
%   value:      The value to be used with gamma/bright
%   vector:     Flag to be set if we are passing in a vector, and expect a
%               vector out
%  
%   Tests each of the passed in images and returns the predictions for each
%   image.            

    if ~exist('values', 'var')
        return;
    end
        
    % get image into correct format for preprocessing
    if exist('vector', 'var')
        image = VectorToGreyscaleImage(image);
    end
    
    [rows, ~] = size(methods);
       
    if ~iscell(methods)       
        if (rows == 1)
             methods = {methods, 'None'};
        end
    end
    % If there's only one value passed in make it into a vector so that
    % comparisons don't occur for each letter of the string
   
    % Preprocess using combination of methods
    for i = 1:size(methods,2)
        % preprocess
        if strcmp(methods(i), 'Gamma')
            image = GammaCorrection(image,values(i));
        elseif strcmp(methods(i), 'GammaDynamic')
            imageMean = mean(image,1);
            imageMean = mean(imageMean,2);
            if(imageMean > 200)
                gammaValue = 2;
            elseif(imageMean <= 100)
                gammaValue = 0.5;
            else gammaValue = 0.8;
            end
            image = GammaCorrection(image,gammaValue);
        elseif strcmp(methods(i), 'sdGamma')
            n = 1;
            Idouble = mat2gray(image);
            avg = mean2(Idouble);
            sigma = std2(Idouble);
             
            lowIn = abs(avg-n*sigma);
            highIn = abs(avg+n*sigma);
                                    
            if( lowIn > 1 )
                lowIn  = 0.9999;
            elseif ( lowIn < 0 )
                lowIn = 0;
            end
                                   
            if( highIn < 0 )
                highIn = 0.0001;
            elseif( highIn > 1 )
                highIn = 1;
            end
                   
            if( lowIn > highIn )
                temp = lowIn;
                lowIn = highIn;
                highIn = temp;
            elseif (lowIn == highIn)
                if(lowIn > 0.001)
                    lowIn = lowIn - 0.0001;
                elseif(highIn < 0.9999)
                    highIn = highIn + 0.0001;
                end
            end
            
            gammaParameters = [lowIn highIn];
            image = GammaCorrectionSD(image,gammaParameters);
        elseif strcmp(methods(i), 'AutoContrast')
            image = AutoContrast(image);
        elseif strcmp(methods(i), 'Hist')
            image = HistogramEqualisation(image);
        elseif strcmp(methods(i), 'Bright')
            image = Brightening(image, values(i));
        end

        % return image to original format
        if exist('vector', 'var')
            image = GreyscaleImageToVector(image);
        end
    end
    
end