#     GABOR     #

Histogram Equalisation with Gabor using NN Classification 
Basic Accuracy: 76.25%
Recall: 0.83
Precision: 0.87831
Specificity: 0.425
FMeasure: 0.85347
False Alarm: 0.575


Histogram Equalisation with Gabor using KNN 3 Classification 
Basic Accuracy: 77.9167%
Recall: 0.87
Precision: 0.86567
Specificity: 0.325
FMeasure: 0.86783
False Alarm: 0.675


Histogram Equalisation with Gabor using KNN 5 Classification 
Basic Accuracy: 80.8333%
Recall: 0.92
Precision: 0.85981
Specificity: 0.25
FMeasure: 0.88889
False Alarm: 0.75


#      Pixel Based Thresholding     #


Histogram Equalisation with Pixel 10 using NN Classification 
Basic Accuracy: 80%
Recall: 0.95
Precision: 0.83333
Specificity: 0.05
FMeasure: 0.88785
False Alarm: 0.95


Histogram Equalisation with Pixel 10 using KNN 3 Classification 
Basic Accuracy: 79.1667%
Recall: 0.95
Precision: 0.82609
Specificity: 0
FMeasure: 0.88372
False Alarm: 1


Histogram Equalisation with Pixel 10 using KNN 5 Classification 
Basic Accuracy: 80%
Recall: 0.96
Precision: 0.82759
Specificity: 0
FMeasure: 0.88889
False Alarm: 1


Histogram Equalisation with Pixel 20 using NN Classification 
Basic Accuracy: 75%
Recall: 0.9
Precision: 0.81818
Specificity: 0
FMeasure: 0.85714
False Alarm: 1


Histogram Equalisation with Pixel 20 using KNN 3 Classification 
Basic Accuracy: 75.8333%
Recall: 0.91
Precision: 0.81982
Specificity: 0
FMeasure: 0.86256
False Alarm: 1


Histogram Equalisation with Pixel 20 using KNN 5 Classification 
Basic Accuracy: 78.3333%
Recall: 0.94
Precision: 0.82456
Specificity: 0
FMeasure: 0.8785
False Alarm: 1


Histogram Equalisation with Pixel 30 using NN Classification 
Basic Accuracy: 70%
Recall: 0.72
Precision: 0.9
Specificity: 0.6
FMeasure: 0.8
False Alarm: 0.4


Histogram Equalisation with Pixel 30 using KNN 3 Classification 
Basic Accuracy: 75.8333%
Recall: 0.81
Precision: 0.89011
Specificity: 0.5
FMeasure: 0.84817
False Alarm: 0.5


Histogram Equalisation with Pixel 30 using KNN 5 Classification 
Basic Accuracy: 79.1667%
Recall: 0.86
Precision: 0.8866
Specificity: 0.45
FMeasure: 0.8731
False Alarm: 0.55


Histogram Equalisation with Pixel 40 using NN Classification 
Basic Accuracy: 60.8333%
Recall: 0.59
Precision: 0.90769
Specificity: 0.7
FMeasure: 0.71515
False Alarm: 0.3


Histogram Equalisation with Pixel 40 using KNN 3 Classification 
Basic Accuracy: 68.3333%
Recall: 0.7
Precision: 0.89744
Specificity: 0.6
FMeasure: 0.78652
False Alarm: 0.4


Histogram Equalisation with Pixel 40 using KNN 5 Classification 
Basic Accuracy: 70%
Recall: 0.74
Precision: 0.88095
Specificity: 0.5
FMeasure: 0.80435
False Alarm: 0.5


Histogram Equalisation with Pixel 50 using NN Classification 
Basic Accuracy: 55.8333%
Recall: 0.52
Precision: 0.91228
Specificity: 0.75
FMeasure: 0.66242
False Alarm: 0.25


Histogram Equalisation with Pixel 50 using KNN 3 Classification 
Basic Accuracy: 63.3333%
Recall: 0.64
Precision: 0.88889
Specificity: 0.6
FMeasure: 0.74419
False Alarm: 0.4


Histogram Equalisation with Pixel 50 using KNN 5 Classification 
Basic Accuracy: 64.1667%
Recall: 0.68
Precision: 0.86076
Specificity: 0.45
FMeasure: 0.75978
False Alarm: 0.55


Histogram Equalisation with Pixel 60 using NN Classification 
Basic Accuracy: 54.1667%
Recall: 0.51
Precision: 0.89474
Specificity: 0.7
FMeasure: 0.64968
False Alarm: 0.3


Histogram Equalisation with Pixel 60 using KNN 3 Classification 
Basic Accuracy: 61.6667%
Recall: 0.62
Precision: 0.88571
Specificity: 0.6
FMeasure: 0.72941
False Alarm: 0.4


Histogram Equalisation with Pixel 60 using KNN 5 Classification 
Basic Accuracy: 62.5%
Recall: 0.64
Precision: 0.87671
Specificity: 0.55
FMeasure: 0.73988
False Alarm: 0.45


Histogram Equalisation with Pixel 70 using NN Classification 
Basic Accuracy: 57.5%
Recall: 0.58
Precision: 0.86567
Specificity: 0.55
FMeasure: 0.69461
False Alarm: 0.45


Histogram Equalisation with Pixel 70 using KNN 3 Classification 
Basic Accuracy: 61.6667%
Recall: 0.63
Precision: 0.875
Specificity: 0.55
FMeasure: 0.73256
False Alarm: 0.45


Histogram Equalisation with Pixel 70 using KNN 5 Classification 
Basic Accuracy: 64.1667%
Recall: 0.67
Precision: 0.87013
Specificity: 0.5
FMeasure: 0.75706
False Alarm: 0.5


Histogram Equalisation with Pixel 80 using NN Classification 
Basic Accuracy: 58.3333%
Recall: 0.62
Precision: 0.83784
Specificity: 0.4
FMeasure: 0.71264
False Alarm: 0.6


Histogram Equalisation with Pixel 80 using KNN 3 Classification 
Basic Accuracy: 60%
Recall: 0.62
Precision: 0.86111
Specificity: 0.5
FMeasure: 0.72093
False Alarm: 0.5


Histogram Equalisation with Pixel 80 using KNN 5 Classification 
Basic Accuracy: 63.3333%
Recall: 0.67
Precision: 0.85897
Specificity: 0.45
FMeasure: 0.75281
False Alarm: 0.55


Histogram Equalisation with Pixel 90 using NN Classification 
Basic Accuracy: 69.1667%
Recall: 0.74
Precision: 0.87059
Specificity: 0.45
FMeasure: 0.8
False Alarm: 0.55


Histogram Equalisation with Pixel 90 using KNN 3 Classification 
Basic Accuracy: 69.1667%
Recall: 0.75
Precision: 0.86207
Specificity: 0.4
FMeasure: 0.80214
False Alarm: 0.6


Histogram Equalisation with Pixel 90 using KNN 5 Classification 
Basic Accuracy: 70%
Recall: 0.75
Precision: 0.87209
Specificity: 0.45
FMeasure: 0.80645
False Alarm: 0.55


Histogram Equalisation with Pixel 100 using NN Classification 
Basic Accuracy: 68.3333%
Recall: 0.73
Precision: 0.86905
Specificity: 0.45
FMeasure: 0.79348
False Alarm: 0.55


Histogram Equalisation with Pixel 100 using KNN 3 Classification 
Basic Accuracy: 68.3333%
Recall: 0.74
Precision: 0.86047
Specificity: 0.4
FMeasure: 0.7957
False Alarm: 0.6


Histogram Equalisation with Pixel 100 using KNN 5 Classification 
Basic Accuracy: 68.3333%
Recall: 0.75
Precision: 0.85227
Specificity: 0.35
FMeasure: 0.79787
False Alarm: 0.65


Histogram Equalisation with Pixel 110 using NN Classification 
Basic Accuracy: 60.8333%
Recall: 0.64
Precision: 0.85333
Specificity: 0.45
FMeasure: 0.73143
False Alarm: 0.55


Histogram Equalisation with Pixel 110 using KNN 3 Classification 
Basic Accuracy: 66.6667%
Recall: 0.71
Precision: 0.86585
Specificity: 0.45
FMeasure: 0.78022
False Alarm: 0.55


Histogram Equalisation with Pixel 110 using KNN 5 Classification 
Basic Accuracy: 69.1667%
Recall: 0.76
Precision: 0.85393
Specificity: 0.35
FMeasure: 0.80423
False Alarm: 0.65


Histogram Equalisation with Pixel 120 using NN Classification 
Basic Accuracy: 63.3333%
Recall: 0.65
Precision: 0.87838
Specificity: 0.55
FMeasure: 0.74713
False Alarm: 0.45


Histogram Equalisation with Pixel 120 using KNN 3 Classification 
Basic Accuracy: 63.3333%
Recall: 0.68
Precision: 0.85
Specificity: 0.4
FMeasure: 0.75556
False Alarm: 0.6


Histogram Equalisation with Pixel 120 using KNN 5 Classification 
Basic Accuracy: 69.1667%
Recall: 0.76
Precision: 0.85393
Specificity: 0.35
FMeasure: 0.80423
False Alarm: 0.65


Histogram Equalisation with Pixel 130 using NN Classification 
Basic Accuracy: 64.1667%
Recall: 0.66
Precision: 0.88
Specificity: 0.55
FMeasure: 0.75429
False Alarm: 0.45


Histogram Equalisation with Pixel 130 using KNN 3 Classification 
Basic Accuracy: 63.3333%
Recall: 0.67
Precision: 0.85897
Specificity: 0.45
FMeasure: 0.75281
False Alarm: 0.55


Histogram Equalisation with Pixel 130 using KNN 5 Classification 
Basic Accuracy: 66.6667%
Recall: 0.72
Precision: 0.85714
Specificity: 0.4
FMeasure: 0.78261
False Alarm: 0.6


Histogram Equalisation with Pixel 140 using NN Classification 
Basic Accuracy: 68.3333%
Recall: 0.71
Precision: 0.8875
Specificity: 0.55
FMeasure: 0.78889
False Alarm: 0.45


Histogram Equalisation with Pixel 140 using KNN 3 Classification 
Basic Accuracy: 69.1667%
Recall: 0.72
Precision: 0.88889
Specificity: 0.55
FMeasure: 0.79558
False Alarm: 0.45


Histogram Equalisation with Pixel 140 using KNN 5 Classification 
Basic Accuracy: 69.1667%
Recall: 0.74
Precision: 0.87059
Specificity: 0.45
FMeasure: 0.8
False Alarm: 0.55


Histogram Equalisation with Pixel 150 using NN Classification 
Basic Accuracy: 65.8333%
Recall: 0.68
Precision: 0.88312
Specificity: 0.55
FMeasure: 0.76836
False Alarm: 0.45


Histogram Equalisation with Pixel 150 using KNN 3 Classification 
Basic Accuracy: 67.5%
Recall: 0.7
Precision: 0.88608
Specificity: 0.55
FMeasure: 0.78212
False Alarm: 0.45


Histogram Equalisation with Pixel 150 using KNN 5 Classification 
Basic Accuracy: 70.8333%
Recall: 0.76
Precision: 0.87356
Specificity: 0.45
FMeasure: 0.81283
False Alarm: 0.55


Histogram Equalisation with Pixel 160 using NN Classification 
Basic Accuracy: 73.3333%
Recall: 0.76
Precision: 0.90476
Specificity: 0.6
FMeasure: 0.82609
False Alarm: 0.4


Histogram Equalisation with Pixel 160 using KNN 3 Classification 
Basic Accuracy: 70.8333%
Recall: 0.75
Precision: 0.88235
Specificity: 0.5
FMeasure: 0.81081
False Alarm: 0.5


Histogram Equalisation with Pixel 160 using KNN 5 Classification 
Basic Accuracy: 77.5%
Recall: 0.81
Precision: 0.91011
Specificity: 0.6
FMeasure: 0.85714
False Alarm: 0.4


Histogram Equalisation with Pixel 170 using NN Classification 
Basic Accuracy: 73.3333%
Recall: 0.76
Precision: 0.90476
Specificity: 0.6
FMeasure: 0.82609
False Alarm: 0.4


Histogram Equalisation with Pixel 170 using KNN 3 Classification 
Basic Accuracy: 80%
Recall: 0.82
Precision: 0.93182
Specificity: 0.7
FMeasure: 0.87234
False Alarm: 0.3


Histogram Equalisation with Pixel 170 using KNN 5 Classification 
Basic Accuracy: 77.5%
Recall: 0.83
Precision: 0.89247
Specificity: 0.5
FMeasure: 0.8601
False Alarm: 0.5


Histogram Equalisation with Pixel 180 using NN Classification 
Basic Accuracy: 67.5%
Recall: 0.71
Precision: 0.87654
Specificity: 0.5
FMeasure: 0.78453
False Alarm: 0.5


Histogram Equalisation with Pixel 180 using KNN 3 Classification 
Basic Accuracy: 76.6667%
Recall: 0.81
Precision: 0.9
Specificity: 0.55
FMeasure: 0.85263
False Alarm: 0.45


Histogram Equalisation with Pixel 180 using KNN 5 Classification 
Basic Accuracy: 80%
Recall: 0.85
Precision: 0.90426
Specificity: 0.55
FMeasure: 0.87629
False Alarm: 0.45


Histogram Equalisation with Pixel 190 using NN Classification 
Basic Accuracy: 72.5%
Recall: 0.74
Precision: 0.91358
Specificity: 0.65
FMeasure: 0.81768
False Alarm: 0.35


Histogram Equalisation with Pixel 190 using KNN 3 Classification 
Basic Accuracy: 73.3333%
Recall: 0.78
Precision: 0.88636
Specificity: 0.5
FMeasure: 0.82979
False Alarm: 0.5


Histogram Equalisation with Pixel 190 using KNN 5 Classification 
Basic Accuracy: 75.8333%
Recall: 0.79
Precision: 0.90805
Specificity: 0.6
FMeasure: 0.84492
False Alarm: 0.4


Histogram Equalisation with Pixel 200 using NN Classification 
Basic Accuracy: 71.6667%
Recall: 0.76
Precision: 0.88372
Specificity: 0.5
FMeasure: 0.8172
False Alarm: 0.5


Histogram Equalisation with Pixel 200 using KNN 3 Classification 
Basic Accuracy: 75%
Recall: 0.79
Precision: 0.89773
Specificity: 0.55
FMeasure: 0.84043
False Alarm: 0.45


Histogram Equalisation with Pixel 200 using KNN 5 Classification 
Basic Accuracy: 74.1667%
Recall: 0.8
Precision: 0.87912
Specificity: 0.45
FMeasure: 0.8377
False Alarm: 0.55


Histogram Equalisation with Pixel 210 using NN Classification 
Basic Accuracy: 69.1667%
Recall: 0.75
Precision: 0.86207
Specificity: 0.4
FMeasure: 0.80214
False Alarm: 0.6


Histogram Equalisation with Pixel 210 using KNN 3 Classification 
Basic Accuracy: 69.1667%
Recall: 0.72
Precision: 0.88889
Specificity: 0.55
FMeasure: 0.79558
False Alarm: 0.45


Histogram Equalisation with Pixel 210 using KNN 5 Classification 
Basic Accuracy: 70%
Recall: 0.75
Precision: 0.87209
Specificity: 0.45
FMeasure: 0.80645
False Alarm: 0.55


Histogram Equalisation with Pixel 220 using NN Classification 
Basic Accuracy: 58.3333%
Recall: 0.62
Precision: 0.83784
Specificity: 0.4
FMeasure: 0.71264
False Alarm: 0.6


Histogram Equalisation with Pixel 220 using KNN 3 Classification 
Basic Accuracy: 60.8333%
Recall: 0.66
Precision: 0.83544
Specificity: 0.35
FMeasure: 0.73743
False Alarm: 0.65


Histogram Equalisation with Pixel 220 using KNN 5 Classification 
Basic Accuracy: 64.1667%
Recall: 0.7
Precision: 0.84337
Specificity: 0.35
FMeasure: 0.76503
False Alarm: 0.65


Histogram Equalisation with Pixel 230 using NN Classification 
Basic Accuracy: 60%
Recall: 0.64
Precision: 0.84211
Specificity: 0.4
FMeasure: 0.72727
False Alarm: 0.6


Histogram Equalisation with Pixel 230 using KNN 3 Classification 
Basic Accuracy: 63.3333%
Recall: 0.71
Precision: 0.82558
Specificity: 0.25
FMeasure: 0.76344
False Alarm: 0.75


Histogram Equalisation with Pixel 230 using KNN 5 Classification 
Basic Accuracy: 65.8333%
Recall: 0.74
Precision: 0.83146
Specificity: 0.25
FMeasure: 0.78307
False Alarm: 0.75


Histogram Equalisation with Pixel 240 using NN Classification 
Basic Accuracy: 60.8333%
Recall: 0.63
Precision: 0.86301
Specificity: 0.5
FMeasure: 0.72832
False Alarm: 0.5


Histogram Equalisation with Pixel 240 using KNN 3 Classification 
Basic Accuracy: 64.1667%
Recall: 0.68
Precision: 0.86076
Specificity: 0.45
FMeasure: 0.75978
False Alarm: 0.55


Histogram Equalisation with Pixel 240 using KNN 5 Classification 
Basic Accuracy: 65.8333%
Recall: 0.71
Precision: 0.85542
Specificity: 0.4
FMeasure: 0.77596
False Alarm: 0.6


Histogram Equalisation with Pixel 250 using NN Classification 
Basic Accuracy: 53.3333%
Recall: 0.48
Precision: 0.92308
Specificity: 0.8
FMeasure: 0.63158
False Alarm: 0.2


Histogram Equalisation with Pixel 250 using KNN 3 Classification 
Basic Accuracy: 55.8333%
Recall: 0.5
Precision: 0.9434
Specificity: 0.85
FMeasure: 0.65359
False Alarm: 0.15


Histogram Equalisation with Pixel 250 using KNN 5 Classification 
Basic Accuracy: 49.1667%
Recall: 0.42
Precision: 0.93333
Specificity: 0.85
FMeasure: 0.57931
False Alarm: 0.15


Histogram Equalisation with Pixel 10 using SVM Classification 
Basic Accuracy: 73.3333%
Recall: 0.77
Precision: 0.89535
Specificity: 0.55
FMeasure: 0.82796
False Alarm: 0.45


Histogram Equalisation with Pixel 20 using SVM Classification 
Basic Accuracy: 70%
Recall: 0.75
Precision: 0.87209
Specificity: 0.45
FMeasure: 0.80645
False Alarm: 0.55


Histogram Equalisation with Pixel 30 using SVM Classification 
Basic Accuracy: 67.5%
Recall: 0.67
Precision: 0.91781
Specificity: 0.7
FMeasure: 0.77457
False Alarm: 0.3


Histogram Equalisation with Pixel 40 using SVM Classification 
Basic Accuracy: 63.3333%
Recall: 0.59
Precision: 0.95161
Specificity: 0.85
FMeasure: 0.7284
False Alarm: 0.15


Histogram Equalisation with Pixel 50 using SVM Classification 
Basic Accuracy: 60%
Recall: 0.55
Precision: 0.94828
Specificity: 0.85
FMeasure: 0.6962
False Alarm: 0.15


Histogram Equalisation with Pixel 60 using SVM Classification 
Basic Accuracy: 66.6667%
Recall: 0.64
Precision: 0.94118
Specificity: 0.8
FMeasure: 0.7619
False Alarm: 0.2

Histogram Equalisation with Pixel 70 using SVM Classification 
Basic Accuracy: 75%
Recall: 0.74
Precision: 0.94872
Specificity: 0.8
FMeasure: 0.83146
False Alarm: 0.2


Histogram Equalisation with Pixel 80 using SVM Classification 
Basic Accuracy: 80.8333%
Recall: 0.8
Precision: 0.96386
Specificity: 0.85
FMeasure: 0.87432
False Alarm: 0.15


Histogram Equalisation with Pixel 90 using SVM Classification 
Basic Accuracy: 80.8333%
Recall: 0.79
Precision: 0.97531
Specificity: 0.9
FMeasure: 0.87293
False Alarm: 0.1


Histogram Equalisation with Pixel 100 using SVM Classification 
Basic Accuracy: 80%
Recall: 0.8
Precision: 0.95238
Specificity: 0.8
FMeasure: 0.86957
False Alarm: 0.2


Histogram Equalisation with Pixel 110 using SVM Classification 
Basic Accuracy: 78.3333%
Recall: 0.77
Precision: 0.9625
Specificity: 0.85
FMeasure: 0.85556
False Alarm: 0.15


Histogram Equalisation with Pixel 120 using SVM Classification 
Basic Accuracy: 75.8333%
Recall: 0.75
Precision: 0.94937
Specificity: 0.8
FMeasure: 0.83799
False Alarm: 0.2


Histogram Equalisation with Pixel 130 using SVM Classification 
Basic Accuracy: 79.1667%
Recall: 0.77
Precision: 0.97468
Specificity: 0.9
FMeasure: 0.86034
False Alarm: 0.1


Histogram Equalisation with Pixel 140 using SVM Classification 
Basic Accuracy: 78.3333%
Recall: 0.75
Precision: 0.98684
Specificity: 0.95
FMeasure: 0.85227
False Alarm: 0.05


Histogram Equalisation with Pixel 150 using SVM Classification 
Basic Accuracy: 82.5%
Recall: 0.81
Precision: 0.9759
Specificity: 0.9
FMeasure: 0.88525
False Alarm: 0.1


Histogram Equalisation with Pixel 160 using SVM Classification 
Basic Accuracy: 81.6667%
Recall: 0.79
Precision: 0.9875
Specificity: 0.95
FMeasure: 0.87778
False Alarm: 0.05


Histogram Equalisation with Pixel 170 using SVM Classification 
Basic Accuracy: 82.5%
Recall: 0.8
Precision: 0.98765
Specificity: 0.95
FMeasure: 0.88398
False Alarm: 0.05


Histogram Equalisation with Pixel 180 using SVM Classification 
Basic Accuracy: 83.3333%
Recall: 0.81
Precision: 0.9878
Specificity: 0.95
FMeasure: 0.89011
False Alarm: 0.05


Histogram Equalisation with Pixel 190 using SVM Classification 
Basic Accuracy: 82.5%
Recall: 0.8
Precision: 0.98765
Specificity: 0.95
FMeasure: 0.88398
False Alarm: 0.05


Histogram Equalisation with Pixel 200 using SVM Classification 
Basic Accuracy: 80.8333%
Recall: 0.77
Precision: 1
Specificity: 1
FMeasure: 0.87006
False Alarm: 0


Histogram Equalisation with Pixel 210 using SVM Classification 
Basic Accuracy: 75%
Recall: 0.7
Precision: 1
Specificity: 1
FMeasure: 0.82353
False Alarm: 0


Histogram Equalisation with Pixel 220 using SVM Classification 
Basic Accuracy: 73.3333%
Recall: 0.7
Precision: 0.97222
Specificity: 0.9
FMeasure: 0.81395
False Alarm: 0.1


Histogram Equalisation with Pixel 230 using SVM Classification 
Basic Accuracy: 70.8333%
Recall: 0.74
Precision: 0.89157
Specificity: 0.55
FMeasure: 0.80874
False Alarm: 0.45


Histogram Equalisation with Pixel 240 using SVM Classification 
Basic Accuracy: 70.8333%
Recall: 0.79
Precision: 0.84946
Specificity: 0.3
FMeasure: 0.81865
False Alarm: 0.7


Histogram Equalisation with Pixel 250 using SVM Classification 
Basic Accuracy: 35%
Recall: 0.23
Precision: 0.95833
Specificity: 0.95
FMeasure: 0.37097
False Alarm: 0.05


#     Raw Pixel      #


Histogram Equalisation with Raw Pixels using SVM Classification 
Basic Accuracy: 72.5%
Recall: 0.67
Precision: 1
Specificity: 1
FMeasure: 0.8024
False Alarm: 0


Histogram Equalisation with Raw Pixels using NN Classification
Basic Accuracy: 69.1667%
Recall: 0.7
Precision: 0.90909
Specificity: 0.65
FMeasure: 0.79096
False Alarm: 0.35


Histogram Equalisation with Raw Pixels using KNN 3 Classification
Basic Accuracy: 69.1667%
Recall: 0.73
Precision: 0.87952
Specificity: 0.5
FMeasure: 0.79781
False Alarm: 0.5


Histogram Equalisation with Raw Pixels using KNN 5 Classification
Basic Accuracy: 68.3333%
Recall: 0.74
Precision: 0.86047
Specificity: 0.4
FMeasure: 0.7957
False Alarm: 0.6