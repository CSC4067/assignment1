%% Setup
clear all
close all

% Create our cross-validation dataset to be used for determining system
% metrics
Kfold = 3;
[firstHalfImages, firstHalfLabels] = loadFaceImages('face_train.cdataset');
[secondHalfImages, secondHalfLabels] = loadFaceImages('face_test.cdataset');
datasetImages = [firstHalfImages; secondHalfImages];
datasetLabels = [firstHalfLabels; secondHalfLabels];
indices = crossvalind('Kfold', datasetLabels, Kfold);

K = 3;



%% Test Batch
fid=fopen('Results.csv','w');
fprintf(fid, 'Pre,\tFeature,\tClass,\tAcc,\tRecall,\tPrecision,\tSpecificity,\tFMeasure,\tFalse\n');

% For-each of the preprocessing methods

for i = {'None', 'Hist', 'AutoContrast', 'sdGamma', 'GammaDynamic',...
        {'Hist','sdGamma'},{'Hist','GammaDynamic'},...  
        {'AutoContrast','sdGamma'},{'AutoContrast','GammaDynamic'}}
    
    % Get this preprocessing method
    preMethod = i{1};
    preValue = 0;
    
    for ii = { 'Raw', 'HoG' }
        % Get the feature method
        featureMethod = ii{1};
        featureValue = 0;

        for iii = { 'KNN', 'SVM' }
            classMethod = iii{1};
            
            % Cross Validation
            TRecall=0; TPrecision=0; TSpecificity=0; TFMeasure=0; TFalseAlarm=0; TBasicAccuracy=0;
            clear model meanX eigenVectors;
            for j=1:Kfold
                testIndices = (indices == j); 
                trainIndices = ~testIndices;

                trainingImages = datasetImages(trainIndices,:);
                testingImages = datasetImages(testIndices,:);

                trainingLabels = datasetLabels(trainIndices,:);
                testingLabels = datasetLabels(testIndices,:);

                [model, meanX, eigenVectors] = Training(trainingImages, trainingLabels, preMethod, preValue, featureMethod, featureValue, classMethod);
                [Recall, Precision, Specificity, FMeasure, FalseAlarm, BasicAccuracy] = Testing(testingImages, testingLabels, model, K, preMethod, preValue, featureMethod, featureValue, classMethod, meanX, eigenVectors);

                TRecall = TRecall+Recall;
                TPrecision = TPrecision+Precision;
                TSpecificity = TSpecificity+Specificity;
                TFMeasure = TFMeasure + FMeasure; 
                TFalseAlarm = TFalseAlarm + FalseAlarm;
                TBasicAccuracy = TBasicAccuracy + BasicAccuracy;  
            end

            TBasicAccuracy = (TBasicAccuracy*100)/3;
            TRecall = TRecall/Kfold;
            TPrecision = TPrecision/Kfold;
            TSpecificity = TSpecificity/Kfold;
            TFMeasure = TFMeasure/Kfold;
            TFalseAlarm = TFalseAlarm/Kfold;

            % Output the results
            if (size(preMethod,2) == 2)
            fprintf(fid, '%s,\t%s,\t%s,\t%.2f,\t%.2f,\t%.2f,\t%.2f,\t%.2f,\t%.2f\n', [preMethod{1},preMethod{2}], featureMethod, classMethod, TBasicAccuracy, TRecall, TPrecision, TSpecificity, TFMeasure, TFalseAlarm);
            else
            fprintf(fid, '%s,\t%s,\t%s,\t%.2f,\t%.2f,\t%.2f,\t%.2f,\t%.2f,\t%.2f\n',preMethod, featureMethod, classMethod, TBasicAccuracy, TRecall, TPrecision, TSpecificity, TFMeasure, TFalseAlarm);
            end
            close all
        end
    end
end
fclose(fid);
