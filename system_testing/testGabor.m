%% Test Script

clear all
close all

[trainingImages, trainingLabels] = loadFaceImages('face_train.cdataset');
[numberOfTrainingImages, sizeOfOneTrainingImage] = size(trainingImages);

trainingVectors = GaborVectors(trainingImages);
%model = SVMtraining(trainingVectors, trainingLabels);
model = NNtraining(trainingVectors, trainingLabels);

[testingImages, testingLabels] = loadFaceImages('face_test.cdataset');
[numberOfTestingImages, sizeOfOneTestingImage] = size(testingImages);

testingVectors = GaborVectors(testingImages);

numTests = size(testingVectors,1);

predictions = zeros(numTests,1);
for i = 1 : numTests
    %[prediction, maxi] = SVMTesting(testingVectors(i,:), model);
    prediction = KNNTesting(testingVectors(i,:), model,1);
    predictions(i) = prediction;
end

[ Recall, Precision, Specificity, FMeasure, FalseAlarm, BasicAccuracy ] = ErrorMeasure( predictions, testingLabels );

disp(['Basic Accuracy: ',num2str(BasicAccuracy*100),'%']);
disp(['Recall: ',num2str(Recall)]);
disp(['Precision: ',num2str(Precision)]);
disp(['Specificity: ',num2str(Specificity)]);
disp(['FMeasure: ',num2str(FMeasure)]);
disp(['False Alarm: ',num2str(FalseAlarm)]);