clear all
close all 

[trainingImages, trainingLabels] = loadFaceImages('face_train.cdataset');
[numberOfTrainingImages, sizeOfOneTrainingImage] = size(trainingImages);

image = trainingImages(1,:);
image = reshape(image,[27 18]);



preprocessed = PreprocessImage(image, 'Hist', 1.5);


%figure
%subplot(1,2,1), imagesc(image), colormap(gray), title('Before Preprocessing'), axis off;
%subplot(1,2,2), imagesc(preprocessed), colormap(gray), title('After Preprocessing'), axis off;
image = uint8(image);
preprocessed = uint8(preprocessed);
DisplayWithHistogram(image);
DisplayWithHistogram(preprocessed);