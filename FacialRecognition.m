%% Step 0 : Setup
clear all
close all

PREPROCESSING_METHOD ={'Hist','sdGamma'};          % 'Gamma', 'Hist', 'Bright', 'GammaDynamic', 'AutoContrast', 'sdGamma'
PREPROCESSING_VALUE = [0,0];                    % The value to be used with the chosen preprocessing method

FEATURE_EXTRACTION_METHOD = 'HoG';      % 'Gabor', 'Pixel', 'Dim', 'Raw', 'HoG'
FEATURE_EXTRACTION_VALUE = 0;          % Value to be used for pixel threshold/dimensions to reduce

CLASSIFICATION_METHOD = 'SVM';          % 'SVM', 'NN' or 'KNN'
K = 50;                                  % K value for KNN

CROSS_VALIDATION = 'Yes';               % 'Yes' to use K-fold Cross Validation
Kfold = 3;
%Cross Validation Dataset
if(strcmp(CROSS_VALIDATION, 'Yes'))
    [firstHalfImages, firstHalfLabels] = loadFaceImages('face_train.cdataset');
    [secondHalfImages, secondHalfLabels] = loadFaceImages('face_test.cdataset');
    datasetImages = [firstHalfImages; secondHalfImages];
    datasetLabels = [firstHalfLabels; secondHalfLabels];
    indices = crossvalind('Kfold', datasetLabels, Kfold);
    TRecall=0; TPrecision=0; TSpecificity=0; TFMeasure=0; TFalseAlarm=0; TBasicAccuracy=0;
    for i=1:Kfold
        testIndices = (indices == i); 
        trainIndices = ~testIndices;
        trainingImages = datasetImages(trainIndices,:);
        testingImages = datasetImages(testIndices,:);
        trainingLabels = datasetLabels(trainIndices,:);
        testingLabels = datasetLabels(testIndices,:);
        [model, meanX, eigenVectors] = Training(trainingImages, trainingLabels, PREPROCESSING_METHOD,PREPROCESSING_VALUE, FEATURE_EXTRACTION_METHOD, FEATURE_EXTRACTION_VALUE, CLASSIFICATION_METHOD);
        [Recall, Precision, Specificity, FMeasure, FalseAlarm, BasicAccuracy] = Testing(testingImages, testingLabels, model, K, PREPROCESSING_METHOD,PREPROCESSING_VALUE, FEATURE_EXTRACTION_METHOD, FEATURE_EXTRACTION_VALUE, CLASSIFICATION_METHOD, meanX, eigenVectors);
        
        fprintf('\n|------------------|\n');
        disp(['|Cross Validation: ',num2str(i), '|']);
        fprintf('|------------------|\n\n');
        disp(['Basic Accuracy: ',num2str(BasicAccuracy*100),'%']);
        disp(['Recall: ',num2str(Recall)]);
        disp(['Precision: ',num2str(Precision)]);
        disp(['Specificity: ',num2str(Specificity)]);
        disp(['FMeasure: ',num2str(FMeasure)]);
        disp(['False Alarm: ',num2str(FalseAlarm)]);
        
        TRecall = TRecall+Recall;
        TPrecision = TPrecision+Precision;
        TSpecificity = TSpecificity+Specificity;
        TFMeasure = TFMeasure + FMeasure; 
        TFalseAlarm = TFalseAlarm + FalseAlarm;
        TBasicAccuracy = TBasicAccuracy + BasicAccuracy;  
    end
    fprintf('\n|-------Average----------|\n');
    disp(['Basic Accuracy: ',num2str(TBasicAccuracy*100/Kfold),'%']);
    disp(['Recall: ',num2str(TRecall/Kfold)]);
    disp(['Precision: ',num2str(TPrecision/Kfold)]);
    disp(['Specificity: ',num2str(TSpecificity/Kfold)]);
    disp(['FMeasure: ',num2str(TFMeasure/Kfold)]);
    disp(['False Alarm: ',num2str(TFalseAlarm/Kfold)]);
else
    %Load Normal Split Dataset and perform testing and training
    [trainingImages, trainingLabels] = loadFaceImages('face_train.cdataset');
    [testingImages, testingLabels] = loadFaceImages('face_test.cdataset');
    %Train the system using Normal Split Dataset
    [model, meanX, eigenVectors] = Training(trainingImages, trainingLabels, PREPROCESSING_METHOD,PREPROCESSING_VALUE, FEATURE_EXTRACTION_METHOD, FEATURE_EXTRACTION_VALUE, CLASSIFICATION_METHOD);
    %Test the system using Normal Split Dataset
    [Recall, Precision, Specificity, FMeasure, FalseAlarm, BasicAccuracy] = Testing(trainingImages, trainingLabels, model, K, PREPROCESSING_METHOD,PREPROCESSING_VALUE, FEATURE_EXTRACTION_METHOD, FEATURE_EXTRACTION_VALUE, CLASSIFICATION_METHOD, meanX, eigenVectors);
    fprintf('\n|-------Results----------|\n');
    disp(['Basic Accuracy: ',num2str(BasicAccuracy*100),'%']);
    disp(['Recall: ',num2str(Recall)]);
    disp(['Precision: ',num2str(Precision)]);
    disp(['Specificity: ',num2str(Specificity)]);
    disp(['FMeasure: ',num2str(FMeasure)]);
    disp(['False Alarm: ',num2str(FalseAlarm)]);
end

%return

%% Step 3 : Detection Implementation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('\n|--------------------|\n');
fprintf('| Step 3 : Detection |\n');
fprintf('|--------------------|\n\n');

for index = 1:4
    % Get the current image and remove it from its cell
    image = imread(['resources/im', num2str(index), '.jpg']);
    
    % Preprocess the full image now (now done on a sample by sample basis)
    %image = PreprocessImage(image, PREPROCESSING_METHOD, PREPROCESSING_VALUE);
    
    disp(['Image ',num2str(index), ':']);

    % Find faces
    boundingBoxes = Slider(image, model, CLASSIFICATION_METHOD, K, FEATURE_EXTRACTION_METHOD, FEATURE_EXTRACTION_VALUE, PREPROCESSING_METHOD, PREPROCESSING_VALUE, meanX, eigenVectors);
    disp(['Boxes before NMS: ',num2str(size(boundingBoxes,1))]);
    DisplayImageAndDetections(image,boundingBoxes);
    
    sortedBoxes = sortrows(boundingBoxes, 5);
    
    % non-maxima supression
    mergedBoxes = NMS(boundingBoxes);
   
    disp(['Boxes after NMS: ',num2str(size(mergedBoxes,1))]);
        
    % Display this image and its boxes
    DisplayImageAndDetections(image,mergedBoxes);
end


